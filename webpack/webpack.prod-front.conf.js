const path = require('path');
const merge = require('webpack-merge');
const baseWebpackConfig = require('./webpack.base.conf');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const FileManagerPlugin = require('filemanager-webpack-plugin');
const WebpackMd5Hash = require('webpack-md5-hash');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const buildWebpackConfig = merge(baseWebpackConfig, {
    mode: 'production',
    plugins: [
        new FileManagerPlugin({
            onEnd: [
                {
                    delete: [
                        "./public/assets/img/icons-svg",
                    ]
                }
            ]
        }),
        new CleanWebpackPlugin({}),
        new WebpackMd5Hash(),
        new HtmlWebpackPlugin({
            filename: './index.html',
            template: './src/twig/pages/index.twig',
        }),
        new HtmlWebpackPlugin({
            filename: './lk.html',
            template: './src/twig/pages/lk.twig',
        }),
        new HtmlWebpackPlugin({
            filename: './gifts.html',
            template: './src/twig/pages/gifts.twig',
        }),
        new HtmlWebpackPlugin({
            filename: './sales.html',
            template: './src/twig/pages/sales.twig',
        }),
        new HtmlWebpackPlugin({
            filename: './no-team.html',
            template: './src/twig/pages/no-team.twig',
        }),
        new HtmlWebpackPlugin({
            filename: './team.html',
            template: './src/twig/pages/team.twig',
        }),
        new HtmlWebpackPlugin({
            filename: './no-tickets.html',
            template: './src/twig/pages/no-tickets.twig',
        }),
        new HtmlWebpackPlugin({
            filename: './tickets.html',
            template: './src/twig/pages/tickets.twig',
        }),
        new HtmlWebpackPlugin({
            filename: './learn-start.html',
            template: './src/twig/pages/learn-start.twig',
        }),
        new HtmlWebpackPlugin({
            filename: './learn-list.html',
            template: './src/twig/pages/learn-list.twig',
        }),
        new HtmlWebpackPlugin({
            filename: './learn-finish.html',
            template: './src/twig/pages/learn-finish.twig',
        }),
        new HtmlWebpackPlugin({
            filename: './learn-page.html',
            template: './src/twig/pages/learn-page.twig',
        }),
        new HtmlWebpackPlugin({
            filename: './rules.html',
            template: './src/twig/pages/rules.twig',
        }),
        new HtmlWebpackPlugin({
            filename: './how.html',
            template: './src/twig/pages/how.twig',
        }),
    ]
});

module.exports = new Promise((resolve, reject) => {
    resolve(buildWebpackConfig)
});