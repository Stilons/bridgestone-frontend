require('../../node_modules/jquery/dist/jquery.min');
require('../../node_modules/select2/dist/js/select2.full.min');
require('../../node_modules/jquery-sticky/jquery.sticky');
require('../../node_modules/inputmask/dist/jquery.inputmask.min');
require('../../node_modules/chart.js/dist/Chart.min');

require('./libs/sticky.js');

require('./scripts');
require('./components/select');
require('./components/sticky');
require('./components/slider');
require('./components/progress-bar');
require('./components/chart');
