let ProgressBar = require('progressbar.js');

// progressbar.js@1.0.0 version is used
// Docs: http://progressbarjs.readthedocs.org/en/1.0.0/

let bar = new ProgressBar.SemiCircle(progressBar, {
    strokeWidth: 3,
    color: '#FF0000',
    trailColor: '#eee',
    trailWidth: 3,
    easing: 'easeInOut',
    duration: 1400,
    svgStyle: null,
    text: {
        value: '',
        alignToBottom: false,
        className: 'progressbar__label',
    },
    from: {color: '#FFEA82'},
    to: {color: '#FF0000'},
    // Set default step function for all animate calls
    step: (state, bar) => {
        bar.path.setAttribute('stroke', state.color);
        var value = Math.round(bar.value() * 5000);
        if (value === 0) {
            bar.setText('');
        } else {
            bar.setText(value + ' КБ');
        }

        bar.text.style.color = state.color;
    }
});
bar.text.style.fontFamily = 'Arial, sans-serif';
bar.text.style.fontSize = '28px';

bar.animate(0.77);  // Number from 0.0 to 1.0