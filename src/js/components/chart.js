var Chart = require('chart.js');

let data = {
    labels: ["", "5 фев", "6 фев", "7 фев", "8 фев", "9 фев", "10 фев", "11 фев"],
    datasets: [{
        fill: false,
        borderColor: "#FF0000",
        borderWidth: 4,
        hoverBorderColor: "rgba(255,99,132,1)",
        data: [,848, 300, 754, 500, 450, 1200, 500],
        label: 'Количество баллов',
        xAxisID: 'xAxis1',
        lineTension: 0
    }]
};


let options = {
    legend: {
        display: false
    },
    responsive: true,
    maintainAspectRatio: false,
    scales: {
        yAxes: [{
            type: 'linear',
            position: 'left',
            gridLines: {
                display: false
            },
            ticks:{
                beginAtZero:true,
                suggestedMin: '0',
                suggestedMax: '900',
            }
        }],
        xAxes:[
            {
                id:'xAxis1',
                type:"category",
                position: 'bottom',
                gridLines: {
                    display: true,
                    color: "rgba(0,0,0,0.1)"
                },
                scaleLabel: {
                    display: true,
                },
            },
        ]
    }
};


Chart.Line('chart', {
    options: options,
    data: data
});