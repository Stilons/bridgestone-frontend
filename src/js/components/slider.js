import slick from 'slick-carousel';

$(document).ready(function () {
    $('.slick').slick({
        infinite: true,
        nextArrow: '<button type="button" class="slick-arrow _next"></button>',
        prevArrow: '<button type="button" class="slick-arrow _prev"></button>',

    });
});
