import './../sass/main.scss';

//Подключаем SVG спрайты
function requireAll(r) {
    r.keys().forEach(r);
}

requireAll(require.context('./../assets/img/icons-svg', true, /\.svg$/));

fetch(`/assets/img/sprite.svg`).then(res => {
    return res.text();
}).then(data => {
    document.getElementById('svg-icons').innerHTML = data;
});

$(document).ready(function () {
    let mobMenu = $('.js-mob-menu'),
        mobMenuBtn = $('.js-mob-menu-btn'),
        overlay = $('.overlay'),
        orderBtn = $('.js-gift-card-buy'),
        orderForm = $('.gift-order'),
        qrModal = $('.js-qr'),
        qrBtn = $('.js-qr-btn'),
        authBtn = $('.js-auth-btn'),
        authForm = $('.js-auth'),
        forgotForm = $('.js-forgot-pass'),
        forgotBtn = $('.js-forgot-btn'),
        regBtn = $('.js-reg-btn'),
        regForm = $('.js-reg'),
        rulesBtn = $('.js-rules-btn'),
        rulesForm = $('.js-rules'),
        noTicketsBtn = $('.js-new-ticket-btn'),
        noTicketsForm = $('.js-new-ticket'),
        TicketsBtn = $('.js-ticket-btn'),
        TicketsForm = $('.js-ticket'),
        TicketsList = $('.js-ticket-list'),
        TicketsBack = $('.js-ticket-back'),
        ExamStartForm = $('.js-start-exam'),
        ExamStartBtn = $('.js-start-exam-btn'),
        regFormBack = $('.js-reg-back'),
        modal = $('.modal'),
        closeBtn = $('.js-close');

    $(mobMenuBtn).click(function () {
        $(mobMenu).addClass('_visible');
        $(overlay).css('display', 'block');
        $('.body').addClass('_modal-open');
    });

    $(overlay).click(function () {
        $(mobMenu).removeClass('_visible');
        $(modal).removeClass('_visible');
        $(overlay).css('display', 'none');
        $('.body').removeClass('_modal-open');
    });

    $(orderBtn).click(function () {
        $(orderForm).addClass('_visible');
        $(overlay).css('display', 'block');
        $('.body').addClass('_modal-open');
    });

    $(authBtn).click(function () {
        $(modal).closest('.modal').removeClass('_visible');
        $(authForm).addClass('_visible');
        $(overlay).css('display', 'block');
        $('.body').addClass('_modal-open');
        $('.container').show();
    });

    $(qrBtn).click(function () {
        $(qrModal).addClass('_visible');
        $('.body').addClass('_modal-open');
    });

    $(closeBtn).on('click', function (e) {
        $(qrModal).removeClass('_visible');
        $(overlay).css('display', 'none');
        $('.body').removeClass('_modal-open');
    });

    $(forgotBtn).on('click', function (e) {
        $(modal).closest('.modal').removeClass('_visible');
        $(forgotForm).addClass('_visible');
        $('.body').removeClass('_modal-open');
    });

    $(rulesBtn).on('click', function (e) {
        $(modal).closest('.modal').removeClass('_visible');
        $(rulesForm).addClass('_visible');
        $(overlay).css('display', 'block');
        $('.body').removeClass('_modal-open');
    });

    $(regBtn).on('click', function (e) {
        $(modal).closest('.modal').removeClass('_visible');
        $(regForm).addClass('_visible');
        $('.container').hide();
    });

    $(noTicketsBtn).on('click', function (e) {
        $(modal).closest('.modal').removeClass('_visible');
        $(noTicketsForm).addClass('_visible');
        $(overlay).css('display', 'block');
    });

    $(ExamStartBtn).on('click', function (e) {
        $(modal).closest('.modal').removeClass('_visible');
        $(ExamStartForm).addClass('_visible');
        $(overlay).css('display', 'block');
    });

    $('.js-chat-btn').on('click', function (e) {
        $('.js-chat').toggleClass('hidden');
    });

    if ($(window).width() <= 767) {
        $(TicketsBtn).on('click', function (e) {
            $(TicketsForm).addClass('_visible');
            $(TicketsList).hide();
        });

        $(TicketsBack).on('click', function (e) {
            $(TicketsForm).removeClass('_visible');
            $(TicketsList).show();
        });
    }

    $(regFormBack).on('click', function (e) {
        $(modal).closest('.modal').removeClass('_visible');
        $('.container').show();
    });

    $('.js-tel').inputmask("+7 (999) 999 99 99");

    $('#copyBtn').click(function() {
        let $temp = $("<input>");
        $("body").append($temp);
        $temp.val($('#textCopy').text()).select();
        document.execCommand("copy");
        $temp.remove();
        $(this).text('Тест скопирован!');
    });
});